const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.json())

const DATA_FILE_PATH = 'data/store.json'

app.set('port', (process.env.PORT || 3001));

// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'))
}

app.get('/nodes', (req, res) => {
  const data = JSON.parse(fs.readFileSync(DATA_FILE_PATH, 'utf-8'))
  res.json(data)
});

app.put('/nodes', function (req, res) {
  fs.writeFileSync(DATA_FILE_PATH, JSON.stringify(req.body))
  res.json(req.body)
})

app.listen(app.get('port'), () => {
  console.log(`Find the server at: http://localhost:${app.get('port')}/`) // eslint-disable-line no-console
});
