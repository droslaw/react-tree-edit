import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Modal, FormGroup, FormControl, Button } from 'react-bootstrap'
import * as actions from '../actions'

const { hideUpdateDialog, updateNode } = actions;

class TreeEdit extends Component
{
  constructor(props) {
    super(props)
      this.state = {
        nodeName: ''
      }
  }
  update() {
    let id = this.props.modal.editedNode ? this.props.modal.editedNode.id : null
    this.props.onEditNode(id, this.state.nodeName)
  }

  onChange(evt) {
    this.setState({
      nodeName: evt.target.value
    })
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      nodeName: nextProps.modal.editedNode ? nextProps.modal.editedNode.name : ''
    })
  }

  render() {
    return (
      <Modal show={this.props.modal.visible} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit tree node</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormGroup>
            <FormControl
              defaultValue={this.state.nodeName}
              type="text"
              onChange={this.onChange.bind(this)}
            />
          </FormGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.update.bind(this)}>Update</Button>
          <Button onClick={this.props.onClose}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    modal: state.modal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClose: () => {
      dispatch(hideUpdateDialog())
    },
    onEditNode: (id, name) => {
      dispatch(updateNode(id, name))
      dispatch(hideUpdateDialog())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TreeEdit);
