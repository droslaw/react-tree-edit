import React, { Component, PropTypes } from 'react';
import '../css/treelist.css';

class TreeList extends Component {
  render() {
    let nodes = this.props.nodes
    return (
      <ul className="list-group tree-list well">
        {nodes.map((eachNode) =>
          <TreeNode key={eachNode.id} nodeData={eachNode} onSelect={this.onSelectNode.bind(this)} selectedNode={this.props.selectedNode}/>
        )}
      </ul>
    );
  }

  onSelectNode(node) {
    this.props.onSelectNode(node ? node.getData() : null);
  }
}

TreeList.propTypes = {
  nodes: PropTypes.array.isRequired,
  selectedNode: PropTypes.object,
  onSelectNode: PropTypes.func.isRequired
};

class TreeNode extends Component {

  render() {
    var nodeData = this.props.nodeData,
      childNodes = [],
      indent = this.props.indent,
      indentStyle = {
        marginLeft: 20*indent
      }
      if (nodeData.children) {
        childNodes = nodeData.children.map((eachNode) =>
          <TreeNode key={eachNode.id} nodeData={eachNode} indent={indent+1} onSelect={this.props.onSelect} selectedNode={this.props.selectedNode}/>
        );
      }

      return(
        <span>
          <li className={this.isActive() ? "list-group-item active" : "list-group-item"} onClick={this.toggleSelected.bind(this)}>
            <span style={indentStyle}></span>
            {nodeData.name}
          </li>
          <ul className="list-group">
            {childNodes}
          </ul>
        </span>

      );
  }

  toggleSelected() {
    this.isActive() ? this.unselect() : this.select();
  }

  getData() {
    return {...this.props.nodeData};
  }

  select() {
    this.props.onSelect(this);
  }

  unselect() {
    this.props.onSelect();
  }

  isActive(active) {
    return this.props.selectedNode && this.props.selectedNode.id === this.props.nodeData.id;
  }
}

TreeNode.propTypes = {
  nodeData: PropTypes.object.isRequired,
  indent: PropTypes.number,
  selectedNode: PropTypes.object,
  onSelect: PropTypes.func.isRequired
};

TreeNode.defaultProps = {
  indent: 0
};

export default TreeList;
