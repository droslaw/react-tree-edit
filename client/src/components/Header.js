import React from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions';

const { deleteSelectedNode, showUpdateDialog, createNode, selectNode } = actions;

const Header = ({selectedNode, clickCreateNode, clickUpdateNode, clickDeleteNode}) => {
  return (
  <div className="panel-heading">
    <button onClick={() => {clickCreateNode(selectedNode)}} type="button" className="btn btn-md btn-primary">
      <span className="glyphicon glyphicon-plus"></span> Add
    </button>
    <button onClick={() => {clickUpdateNode(selectedNode)}}
            type="button"
            className={selectedNode ? "btn btn-md btn-primary" : "btn btn-md btn-primary disabled"}>

      <span className="glyphicon glyphicon-edit"></span> Update
    </button>
    <button onClick={() => {clickDeleteNode(selectedNode)}}
            type="button"
            className={selectedNode ? "btn btn-md btn-danger" : "btn btn-md btn-danger disabled"}>

      <span className="glyphicon glyphicon-remove"></span> Delete
    </button>
  </div>
)}

const mapStateToProps = (state) => {
  return {
    selectedNode: state.tree.selectedNode
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clickCreateNode: (parentNode) => {
      dispatch(createNode(parentNode))
    },
    clickUpdateNode: (node) => {
      if (!node) {
        return
      }
      dispatch(showUpdateDialog(node))
    },
    clickDeleteNode: (node) => {
      if (!node) {
        return
      }
      dispatch(deleteSelectedNode(node))
      dispatch(selectNode(null))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
