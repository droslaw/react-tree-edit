import React from 'react';
import { connect } from 'react-redux'
import { saveData } from '../actions'

const Footer = ({isModified, data, clickSave}) => {
return (
    <div className="panel-heading">
        <button onClick={() => {clickSave(data, isModified)}}
                type="button"
                className={isModified ? "btn btn-md btn-primary" : "btn btn-md btn-primary disabled"}>

            <span className="glyphicon glyphicon-save"></span> Save
        </button>
    </div>
)}

const mapStateToProps = (state) => {
    return {
        data: state.tree.nodes,
        isModified: state.tree.modified
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clickSave: (data, isModified) => {
            if (!isModified) {
                return
            }
            dispatch(saveData(data))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Footer);
