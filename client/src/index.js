import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { fetchData } from './actions'
import app from './reducers'
import App from './containers/App'
import './css/index.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap-theme.css'

let nodes = [
]
let state = {
  tree: {
    nodes: nodes,
    selectedNode: null,
    modified: false
  },
  modal: {
    visible: false,
    editedNode: null
  }
}

let store = createStore(app, state, applyMiddleware(thunk))

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);

store.dispatch(fetchData())