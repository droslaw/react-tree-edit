import React from 'react';
import VisibleTreeList from './VisibleTreeList';
import TreeEdit from '../components/TreeEdit';
import Header from '../components/Header';
import Footer from '../components/Footer';


const App = () => (
  <span>
    <div className="panel panel-default">
      <Header/>
      <div className="panel-body">
        <VisibleTreeList />
      </div>
    </div>
    <TreeEdit />
    <Footer />
  </span>
)

export default App;
