import { connect } from 'react-redux'
import TreeList from '../components/TreeList'
import {selectNode} from '../actions'

const mapStateToProps = (state) => {
  return {
    nodes: state.tree.nodes,
    selectedNode: state.tree.selectedNode
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSelectNode: (node) => {
      dispatch(selectNode(node))
    }
  }
}

const VisibleTreeList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TreeList)

export default VisibleTreeList
