import fetch from 'isomorphic-fetch'
import { API_URL } from '../config'

export const CREATE_NODE = 'CREATE_NODE'
export const DELETE_NODE = 'DELETE_NODE'
export const UPDATE_NODE = 'UPDATE_NODE'
export const SELECT_NODE = 'SELECT_NODE'
export const SHOW_MODAL = 'SHOW_MODAL'
export const HIDE_MODAL = 'HIDE_MODAL'
export const REQUEST = 'REQUEST'
export const RECEIVE_DATA = 'RECEIVE_DATA'

let nextId = 1000;

const flattenNodes = (tree, pid = null) => {
    let treeList = []
    if (!tree.length) {
        return [];
    }
    tree.forEach((node) => {
        treeList.push({
            id: node.id,
            name: node.name,
            pid:pid
        })
        if (node.children && node.children.length) {
            let childNodes = flattenNodes(node.children, node.id)
            treeList = [...treeList, ...childNodes]
        }
    })
    return treeList
}

export const createNode = (parentNode) => ({
  type: CREATE_NODE,
  parentNode: parentNode,
  node: {
    id: nextId++,
    name: 'test'
  }
});

export const deleteSelectedNode = (node) => ({
  type: DELETE_NODE,
  node: node
});

export const updateNode = (id, name) => ({
  type: UPDATE_NODE,
  id: id,
  name: name
});

export const selectNode = (node) => ({
  type: SELECT_NODE,
  node: node
});

export const showUpdateDialog = (node) => ({
  type: SHOW_MODAL,
  node: node
})

export const hideUpdateDialog = () => ({
  type: HIDE_MODAL
})

export const request = () => ({
    type: REQUEST
})

export const receiveData = (data) => {
    return {
        type: RECEIVE_DATA,
        data: data
    }
}

export const fetchData = () => {
    return dispatch => {
        dispatch(request())
        return fetch(API_URL)
            .then(response => response.json())
            .then(json => dispatch(receiveData(json)))
    }
}

export const saveData = (data) => {
    return dispatch => {
        dispatch(request())
        return fetch(API_URL,
            {
              method: 'PUT',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(flattenNodes(data))
            })
            .then(response => response.json())
            .then(json => dispatch(receiveData(json)))
    }
}

