import { combineReducers } from 'redux'
import { SELECT_NODE, SHOW_MODAL, HIDE_MODAL } from '../actions'
import { nodeReducers, isTreeModified } from './nodes'


const nodes = (state = {}, action) => {
  let reducer = nodeReducers[action.type]
  if (reducer) {
    return reducer(state, action)
  }
  return state
}

const selectedNode = (state = {}, action) => {
  if (SELECT_NODE === action.type) {
    return action.node
  }
  return state
}

const modal = (state = {}, action) => {
  switch (action.type) {
  case SHOW_MODAL:
    return {
      visible: true,
      editedNode: action.node
    }
    case HIDE_MODAL:
      return {
        visible: false,
        editedNode: null
      }
    default:
      return state
  }
}

const tree = (state = {}, action) => {
  let resultNodes = nodes(state.nodes, action)
  return {
    nodes: resultNodes,
    selectedNode: selectedNode(state.selectedNode, action),
    modified: isTreeModified(resultNodes)
  }
}

export default combineReducers({
  tree,
  modal
})
