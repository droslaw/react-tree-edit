import { CREATE_NODE, DELETE_NODE, UPDATE_NODE, RECEIVE_DATA } from '../actions'

const filterTree = (nodes, filterCondition) => {
  let result = []
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i],
      currentResult = {}
    if (!filterCondition(node)) {
      continue;
    }
    currentResult = {
      id: node.id,
      name: node.name
    }
    if (node.children) {
      let children = filterTree(node.children, filterCondition)
      if (children) {
        currentResult.children = children
      }
    }
    result.push(currentResult)
  }
  return result
}

const addChild = (nodes, childNode, parentId) => {
  let result = []
  if (!nodes) {
    return result;
  }
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i],
      currentResult = {
        id: node.id,
        name: node.name
      }
    currentResult.children = addChild(node.children, childNode, parentId)
    if (node.id === parentId) {
      currentResult.children.push(childNode)
    }
    result.push(currentResult)
  }
  return result
}

const updateNode = (nodes, id, name) => {
  let result = []
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i],
      currentResult
    if (node.id === id) {
      currentResult = {
        id: node.id,
        name: name
      }
    } else {
      currentResult = {
        id: node.id,
        name: node.name
      }
    }
    if (node.children && node.children.length) {
      currentResult.children = updateNode(node.children, id, name)
    }
    result.push(currentResult)
  }
  return result
}

const buildTree = (nodesList) => {
  let tree = []
  let allNodes = {}
  if (!nodesList.length) {
    return [];
  }
  nodesList.forEach((node) => {
    allNodes[node.id] = {
        id: node.id,
        name: node.name,
        children: []
    }
  })
  nodesList.forEach((node) => {
    if (null != node.pid) {
        allNodes[node.pid].children.push(allNodes[node.id])
    } else {
      tree.push(allNodes[node.id])
    }
  })
  return tree
}

const cloneTree = (nodes) => {
  let result = []
  for (let i = 0; i < nodes.length; i++) {
    let node = nodes[i],
      currentResult = {}

    currentResult = {
      id: node.id,
      name: node.name
    }
    if (node.children) {
      let children = cloneTree(node.children)
      if (children) {
        currentResult.children = children
      }
    }
    result.push(currentResult)
  }
  return result
}

let initialNodes = [];

export const isTreeModified = (currentState) => {
  return JSON.stringify(currentState) !== JSON.stringify(initialNodes)
}

export const nodeReducers = {
  [CREATE_NODE]: (state = [], action) => {
    let parentId = action.parentNode ? action.parentNode.id : null
    if (!parentId) {
        return [...addChild(state, action.node, parentId), action.node]
    }
    return addChild(state, action.node, parentId)
  },
  [UPDATE_NODE]: (state = [], action) => {
    return updateNode(state, action.id, action.name)
  },
  [DELETE_NODE]: (state = [], action) => {
    if (!action.node) {
        return state
    }
    return filterTree(state, (node) => {return node.id !== action.node.id})
  },
  [RECEIVE_DATA]: (state = [], action) => {
    let tree = buildTree(action.data)
    initialNodes = cloneTree(tree)
    return tree;
  }
}
